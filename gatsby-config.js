module.exports = {
  siteMetadata: {
    title: 'Eddy Fabery | Fullstack Developer',
    author: 'Eddy Fabery',
    description: 'Eddy Fabery',
    siteUrl: 'https://eddyfabery.com'
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'Eddy Fabery Personal',
        short_name: 'Eddy Fabery website',
        start_url: '/',
        background_color: '#663399',
        theme_color: '#663399',
        display: 'minimal-ui',
        icon: 'src/images/favicon/snocircle.png', // This path is relative to the root of the site.
      },
    },
    'gatsby-plugin-sass',
    'gatsby-plugin-sitemap'
  ],
}
