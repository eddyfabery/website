import React from 'react'

export default function IntroPage() {
  if (typeof window !== 'undefined') {
    window.location = '/?q=intro'
  }

  return null
}
