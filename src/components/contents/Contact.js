import React from 'react'
import emailjs from 'emailjs-com';

export default function Contact({ article, close, articleTimeout }) {

  function sendEmail(e) {
    e.preventDefault();

    emailjs.sendForm('default_service', 'template_Tjth4k7r', e.target, 'user_5rFqKCpuXROBv4hFZnSMj')
      .then((result) => {
          console.log(result.text);
      }, (error) => {
          console.log(error.text);
      });
  }

  return (
    <article
      id="contact"
      className={`${article === 'contact' ? 'active' : ''} ${
        articleTimeout ? 'timeout' : ''
      }`}
      style={{ display: 'none' }}
    >
      <h2 className="major">Contact</h2>
      <p>
        If you like my work and have some cool project to work on, just send me
        a message!
      </p>
      <form method="post" action="#" onSubmit={sendEmail}>
        <div className="field half first">
          <label htmlFor="name">Name</label>
          <input type="text" name="name" id="name" />
        </div>
        <div className="field half">
          <label htmlFor="email">Email</label>
          <input type="text" name="email" id="email" />
        </div>
        <div className="field">
          <label htmlFor="message">Message</label>
          <textarea name="message" id="message" rows="4"></textarea>
        </div>
        <ul className="actions">
          <li>
            <input type="submit" value="Send Message" className="special" />
          </li>
          <li>
            <input type="reset" value="Reset" />
          </li>
        </ul>
      </form>
      <ul className="icons">
        <li>
          <a
            href="https://linkedin.com/in/eddyfabery"
            target="_blank"
            className="icon fa-linkedin"
          >
            <span className="label">Linkedin</span>
          </a>
        </li>
        <li>
          <a
            href="https://github.com/eddyfabery"
            target="_blank"
            className="icon fa-github"
          >
            <span className="label">GitHub</span>
          </a>
        </li>
      </ul>
      {close}
    </article>
  )
}
