import React from 'react'

export default function Work({ article, close, articleTimeout }) {
  return (
    <article
      id="work"
      className={`${article === 'work' ? 'active' : ''} ${
        articleTimeout ? 'timeout' : ''
      }`}
      style={{ display: 'none' }}
    >
      <h2 className="major">Work</h2>
      <div>
        <p>
          Below are some of the companies I've worked for where I cherish my
          experiences!
        </p>
      </div>
      <hr />
      <div>
        <div className="company">
          <a href="https://www.backstage.com/" target="_blank">
            <h5>Backstage</h5>
          </a>
          <span className="company-duration">2017-Present</span>
        </div>
        <p>
          At Backstage, I've spent a tonne of time optimizing performance
          challenges with page load times, to refactoring and overhauling
          elasticsearch wrappers, to various one-off features :)
        </p>
        A few of the challenges I've enjoyed at my time here:
        <ul>
          <li>
            Identified and resolved scalability bottlenecks found in
            elasticsearch and React front end & Django API.
          </li>
          <li>
            Spearheading Elasticsearch Upgrade from 1.3 to 7.3 addressing
            breaking changes and refactoring for performance.
          </li>
          <li>
            Implement various client facing features and also internal tools!
          </li>
        </ul>
      </div>
      <hr />
      <div>
        <div className="company">
          <a href="https://www.adlightning.com/" target="_blank">
            <h5>AdLightning</h5>
          </a>
          <span className="company-duration">2017-2019</span>
        </div>
        <p>
          Ad Lightning is an ad intelligence platform to shed light on the dark
          side of the ad marketplace.
        </p>
        <p>
          At Adlightning, I've learned a tonne from very experienced developers
          with 20+ of experience. I've enjoyed my time developing performant,
          scalable systems with low maintenance.
        </p>
        A few of the challenges I've enjoyed at my time here:
        <ul>
          <li>
            Process tera-bytes of ad performance data output from Custom Chrome
            that scans our publisher sites.
          </li>
          <li>
            Developing scalable alerting system for malicious ads using AWS
            SNS/SES/Lambda/Dymano.
          </li>
          <li>
            Writing efficient code in Tornado API with no ORM, and displaying
            aggregated metrics via React using ChartJS, ReduxSaga.
          </li>
          <li>
            Implement a GDPR feature to categorize compliant ad data in the
            backend and shed light to publishers in a clean UI!.
          </li>
          <li>
            Implement efficient algorithms to efficiently catch bad domains
            real-time.
          </li>
        </ul>
      </div>
      <hr />
      <div>
        <div className="company">
          <a href="https://www.handy.com/" target="_blank">
            <h5>Handy</h5>
          </a>
          <span className="company-duration">2014-2016</span>
        </div>
        <p>
          Handy is an online two-sided marketplace for residential cleaning,
          installation, and other home services.
        </p>
        <p>
          At Handy, as a 12th engineer on the team seeing through +150M of
          funding and 50+ engineers, I've enjoyed the fast-paced dynamics of a
          company, and learning a lot from various talented engineers from
          everywhere. Not to mention, meeting some life-long friends! Pivoting
          to meet product requirements, to being intertwined with inner-workings
          of a fast growing company, I've also picked up valuable skills here.
        </p>
        A few of the challenges I've enjoyed at my time here:
        <ul>
          <li>
            Implement features to anayze provider location and penalize
            providers based on location data.
          </li>
          <li>Help untangle rails monolith into microservices.</li>
          <li>
            Write advanced SQL queries, utilize HDFS & Airflow & Hive to create
            partitioned clickstream data.
          </li>
          <li>
            Maintain and develop python reporting framework to generate
            Finance/Product/Marketing KPIs.
          </li>
          <li>As an early adopter of Looker, awesome looker things.</li>
        </ul>
      </div>
      {close}
    </article>
  )
}
