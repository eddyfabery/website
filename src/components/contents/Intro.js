import React from 'react'
import snoconeImg from '../../images/snocone.jpeg'

export default function Project({ article, close, articleTimeout }) {
  return (
    <article
      id="intro"
      className={`${article === 'intro' ? 'active' : ''} ${
        articleTimeout ? 'timeout' : ''
      }`}
      style={{ display: 'none' }}
    >
      <h2 className="major">Intro</h2>
      <span className="image main">
        <img src={snoconeImg} alt="" />
      </span>
      <p>
        I'm a Seattle based fullstack developer with 8 years of professional
        experience in developing scalable systems. I'm interested in all kinds of products, but my current
        focus is on devising solutions for the health care industry. I enjoy
        crafting clean code, bouncing ideas with others and most importantly, learning from other talented engineers.
      </p>
      <p>
        You can often find me developing new features or heads down debugging issues. :)
      </p>
      <p>
        I love running/hiking with my pup Snocone (2yr old Samoyed), playing pick-up soccer in the wee hours of the morning, or
        cooking a massive porterhouse steak on the rooftop.
      </p>
      {close}
    </article>
  )
}
