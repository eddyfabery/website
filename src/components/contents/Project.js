import React from 'react'
import mixlabImg from '../../images/mixlab.png'
import signalPlusImg from '../../images/signalplus.png'
import postyPandaImg from '../../images/postypanda.png'

export default function Project({ article, close, articleTimeout }) {
  return (
    <article
      id="projects"
      className={`${article === 'projects' ? 'active' : ''} ${
        articleTimeout ? 'timeout' : ''
      }`}
      style={{ display: 'none' }}
    >
      <h2 className="major">Projects</h2>

      <p>
        Unfortunately, some of the projects have been discontinued so bear with
        me!
      </p>

      <div>
        <a href="https://www.mixlabrx.com/" target="_blank">
          <h5>Mixlab</h5>
        </a>
        <span className="image main">
          <img src={mixlabImg} alt="" />
        </span>
        <p>
          Mixlab is a compounding veterinery pharmacy for pets. I was contracted
          to develop the project from scratch utilizing React & Rails, and
          onboard 4 engineers. Since then, the company has raised +8.5M in
          funding.
        </p>
        I really enjoyed architecting and implementing:
        <ul>
          <li>Real-time order management system for pharmacies</li>
          <li>Patient portals for subscription delivery management</li>
          <li>Pharmacy portal for order management</li>
        </ul>
      </div>

      <hr />
      <div>
        <a href="https://github.com/signalplus-dev/signalplus/" target="_blank">
          <h5>SignalPlus</h5>
        </a>
        <span className="image main">
          <img src={signalPlusImg} alt="" />
        </span>
        <p>
          SignalPlus is a marketing automation platform for small/medium size
          companies. A co-worker/mentor of mine along with a Twitter designer
          were contracted out to build the initial POC utilizing React, Rails,
          Redis, Sidekiq, Stripe, and Twitter Streaming API.
        </p>
      </div>

      <hr />
      <div>
        <h5>PostyPanda</h5>
        <div className="project-image">
          <span className="image left">
            <img src={postyPandaImg} alt="" />
          </span>
        </div>
        <p>
          PostyPanda is a mobile message-board platform providing an anonymous
          place to share ideas, ask questions and raise issues at your
          workplace! Sadly, it's not in use anymore. A co-worker friend and I
          built this with Objective-C, and Ruby on Rails API.
        </p>
      </div>
      {close}
    </article>
  )
}
