import PropTypes from 'prop-types'
import React from 'react'
import Work from './contents/Work'
import Contact from './contents/Contact'
import Project from './contents/Project'
import Intro from './contents/Intro'

class Main extends React.Component {
  render() {
    const {
      article,
      articleTimeout,
      onCloseArticle,
      setWrapperRef,
      timeout,
    } = this.props

    let close = (
      <a
        className="close"
        onClick={() => {
          onCloseArticle()
        }}
        role="button"
      ></a>
    )

    return (
      <div
        ref={setWrapperRef}
        id="main"
        style={timeout ? { display: 'flex' } : { display: 'none' }}
      >
        <Intro
          article={article}
          close={close}
          articleTimeout={articleTimeout}
        />

        <Work article={article} close={close} articleTimeout={articleTimeout} />

        <Project
          article={article}
          close={close}
          articleTimeout={articleTimeout}
        />

        <Contact
          article={article}
          close={close}
          articleTimeout={articleTimeout}
        />
      </div>
    )
  }
}

Main.propTypes = {
  route: PropTypes.object,
  article: PropTypes.string,
  articleTimeout: PropTypes.bool,
  onCloseArticle: PropTypes.func,
  timeout: PropTypes.bool,
  setWrapperRef: PropTypes.func.isRequired,
}

export default Main
