import mixpanel from 'mixpanel-browser'
mixpanel.init('db7cf061d3bae71d65ed593e35a0bea7')

// Enable for all for now
let ENABLE = process.env.NODE_ENV === 'production';

let actions = {
  track: (name, props) => {
    if (ENABLE) mixpanel.track(name, props)
  },
}

export let Mixpanel = actions
