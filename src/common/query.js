export const parseQuery = (qs) => {
  if (!qs) {
    return {};
  }

  const query = {};
  const pairs = (qs[0] === "?" ? qs.substr(1) : qs).split("&");
  for (var i = 0; i < pairs.length; i++) {
    const pair = pairs[i].split("=");
    query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || "");
  }
  return query;
};
